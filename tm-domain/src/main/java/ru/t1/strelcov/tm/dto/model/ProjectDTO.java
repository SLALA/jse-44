package ru.t1.strelcov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "tm_project")
@Entity
@Getter
@Setter
@NoArgsConstructor
public final class ProjectDTO extends AbstractBusinessEntityDTO {

    public ProjectDTO(@NotNull String userId, @NotNull String name) {
        super(userId, name);
    }

    public ProjectDTO(@NotNull String userId, @NotNull String name, @Nullable String description) {
        super(userId, name, description);
    }

}

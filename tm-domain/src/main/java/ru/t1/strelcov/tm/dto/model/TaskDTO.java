package ru.t1.strelcov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Table(name = "tm_task")
@Entity
@Getter
@Setter
@NoArgsConstructor
public final class TaskDTO extends AbstractBusinessEntityDTO {

    @Column(name = "project_id")
    @Nullable
    private String projectId;

    public TaskDTO(@NotNull String userId, @NotNull String name) {
        super(userId, name);
    }

    public TaskDTO(@NotNull String userId, @NotNull String name, @Nullable String description) {
        super(userId, name, description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TaskDTO task = (TaskDTO) o;
        return Objects.equals(projectId, task.projectId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), projectId);
    }

}

package ru.t1.strelcov.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getDataBaseUserLogin());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getDataBaseUserPassword());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getDataBaseUrl());
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDataBaseDriver());
        settings.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getHibernateDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getHibernateHbm2Ddl());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getHibernateShowSql());
        if ("true".equals(propertyService.getHibernateShowSql()))
            settings.put(org.hibernate.cfg.Environment.FORMAT_SQL, "true");
        Logger.getLogger("org.hibernate").setLevel(Level.parse(propertyService.getHibernateLogLevel()));
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
